
var newsFeed = $('#container #news-feed');
var leftSide = $('#container #left-side');
var rightSide = $('#container #right-side');

this.tweetInteractions = {
    onLike : function() {
        $('.per-tweet .tweet-operation .like').click(function() {
            console.log('like');
        });
    },

    onShare: function() {
        $('.per-tweet .tweet-operation .re-tweet').click(function() {
            console.log('re tweet');

        });
    },

    onComment: function() {
        $('.per-tweet .tweet-operation .reply').click(function() {
            console.log('reply');
        });
    }
}

this.userEvents = {
    onTweet: function() {
        $('.tweet-box .tweet-field', newsFeed).on('click', function() {
            console.log("ACTIVE!");
        });
    }
}

this.tweetInteractions.onLike();
this.tweetInteractions.onShare();
this.tweetInteractions.onComment();

this.userEvents.onTweet();
