package com.tweet.twitter.repo;

import com.tweet.twitter.model.Tweets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TweetRepository extends CrudRepository<Tweets, Long> {
}