package com.tweet.twitter.config;

import com.tweet.twitter.model.CustomUserDetails;
import com.tweet.twitter.model.Users;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

public class AuthenticationInfo {
    private Authentication auth;
    private CustomUserDetails user;

    protected String getSessionID () {
        auth = SecurityContextHolder.getContext().getAuthentication();
        WebAuthenticationDetails detailName = (WebAuthenticationDetails) auth.getDetails();
        return detailName.getSessionId();
    }

    protected boolean checkUserLogged () {
        auth = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails cus = new CustomUserDetails();
        if (cus.getClass() == auth.getPrincipal().getClass()) {
            user = (CustomUserDetails) auth.getPrincipal();
            return true;
        }
        return false;
    }

    public Users getUserObject () {
        auth = SecurityContextHolder.getContext().getAuthentication();
        user = (CustomUserDetails) auth.getPrincipal();
        return user.getuser();
    }

    public Authentication getPrincipal () {
        auth = SecurityContextHolder.getContext().getAuthentication();
        return auth;
    }

    protected String getUserName () {
        if (user == null)
            return null;
        return user.getUsername();
    }

    protected Long getUserid () {

        return user.getId();
    }

    protected String getClassName () {
        auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getPrincipal().getClass().getName();
    }

    protected Users getUsers () {
        auth = SecurityContextHolder.getContext().getAuthentication();
        return (Users) auth.getPrincipal();
    }
}
