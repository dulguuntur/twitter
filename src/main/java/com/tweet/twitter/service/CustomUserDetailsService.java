package com.tweet.twitter.service;

import com.tweet.twitter.model.CustomUserDetails;
import com.tweet.twitter.model.Users;
import com.tweet.twitter.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.logging.Logger;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    public CustomUserDetailsService(UsersRepository repository) {
        super();
        this.usersRepository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Logger logger = Logger.getLogger(this.getClass().getName());
        Optional<Users> byName = usersRepository.findByUsername(s);
        byName.orElseThrow(() -> new UsernameNotFoundException("Username not found") );
        logger.info(byName.map(CustomUserDetails::new).get().getEmail());
        return byName.map(CustomUserDetails::new).get();
    }
}
