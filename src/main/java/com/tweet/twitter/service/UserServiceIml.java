package com.tweet.twitter.service;

import com.tweet.twitter.model.Users;
import com.tweet.twitter.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class UserServiceIml implements UserService {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private UsersRepository repository;

    public UserServiceIml() {

    }

    @Autowired
    public UserServiceIml(UsersRepository repository) {
        super();
        this.repository = repository;
    }
    @Override
    public List<Users> getAllUsers() {
        List<Users> list = new ArrayList();
        repository.findAll(sortByIdAsc()).forEach(e -> list.add(e));
        return list;
    }

    @Override
    public boolean getUserByEmail(String email) {
        Optional<Users> user = repository.findByEmail(email);

        return user.isPresent();
    }

    public Users getUserById(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public Users getUserByUsername(String username) {
        return repository.findByUsername(username).get();
    }

    @Override
    public boolean saveOrUpdateSurvey(Users user) {
        try {
            repository.save(user);
            return true;
        } catch (Exception ex) {
            logger.info(ex.toString());
            logger.info(ex.getMessage());
            logger.info(ex.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public boolean deleteUsersById(Long id) {
        return false;
    }

    @Override
    public Integer getFollowers(Users user) {
        List<Users> allUsers = this.getAllUsers();
        return 0;
    }

    @Override
    public boolean saveFollow (Users follow, Users user) {

        //repository.save(new Users(user, follow));

        return true;
    }

    private Sort sortByIdAsc() {
        return new Sort(Sort.Direction.DESC, "id");
    }
}
