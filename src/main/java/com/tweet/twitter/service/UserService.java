package com.tweet.twitter.service;

import com.tweet.twitter.model.Users;
import java.util.List;
import java.util.Optional;

public interface UserService {

    public List getAllUsers();

    public boolean getUserByEmail(String email);
    public Users getUserByUsername(String username);

    public boolean saveOrUpdateSurvey(Users user);
    public Integer getFollowers(Users user);
    boolean saveFollow (Users user, Users follow);
    public boolean deleteUsersById(Long id);
}
