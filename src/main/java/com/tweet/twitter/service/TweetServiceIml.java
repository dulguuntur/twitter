package com.tweet.twitter.service;

import com.tweet.twitter.model.Tweets;
import com.tweet.twitter.model.Users;
import com.tweet.twitter.repo.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TweetServiceIml implements  TweetService {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    TweetRepository tweetRepository;

    @Override
    public Tweets save(Tweets tweets) {
        return tweetRepository.save(tweets);
    }

    @Override
    public List<Tweets> getAllTweets() {
        List<Tweets> tw = new ArrayList<>();
        tweetRepository.findAll().forEach(e -> tw.add(e));
        return tw;
    }

    public List<Tweets> calcTweets (Users user) {
        List<Tweets> allTweets = this.getAllTweets();
        List<Users> followingList = user.getFollowing();

        return allTweets.stream()
                .filter(e -> (followingList.stream()
                        .filter(d -> d.getId() == e.getUser().getId())
                        .count())>0)
                .collect(Collectors.toList());
    }

    private Sort sortByIdAsc() {
        return new Sort(Sort.Direction.DESC, "id");
    }
}
