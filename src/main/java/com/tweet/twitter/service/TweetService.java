package com.tweet.twitter.service;

import com.tweet.twitter.model.Tweets;
import com.tweet.twitter.model.Users;

import java.util.List;

public interface TweetService {
    Tweets save(Tweets tweets);
    List getAllTweets();
    List<Tweets> calcTweets (Users user);
}
