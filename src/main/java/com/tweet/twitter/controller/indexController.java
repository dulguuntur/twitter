package com.tweet.twitter.controller;

import com.tweet.twitter.config.AuthenticationInfo;
import com.tweet.twitter.model.Tweets;
import com.tweet.twitter.repo.UsersRepository;
import com.tweet.twitter.service.TweetService;
import com.tweet.twitter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.logging.Logger;

@Controller
public class indexController extends AuthenticationInfo {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    UserService userrepo;

    TweetService tweetService;

    @Autowired
    public indexController(UserService userService, TweetService tweetService) {
        this.userrepo = userService;
        this.tweetService = tweetService;
    }

    @GetMapping("/")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        if (!checkUserLogged()) {
            model.addAttribute("notauth", "false");
        } else {
            model.addAttribute("auth", getUserName());
            model.addAttribute("user", getUserObject());

            List<Tweets> tweetsOfusers = this.tweetService.calcTweets(getUserObject());
//            logger.info("size : " + tweetsOfusers.size());
//            for (Tweets tw : tweetsOfusers) {
//                logger.info("POST : " + tw.getPosts());
//                logger.info("USER ID : " + Long.toString(tw.getUser().getId()));
//            }
            model.addAttribute("tweets", tweetsOfusers);
        }

        return "index";
    }

}
