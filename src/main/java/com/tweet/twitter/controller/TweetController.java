package com.tweet.twitter.controller;

import com.tweet.twitter.config.AuthenticationInfo;
import com.tweet.twitter.model.Tweets;
import com.tweet.twitter.model.Users;
import com.tweet.twitter.service.TweetService;
import com.tweet.twitter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@Controller
public class TweetController extends AuthenticationInfo {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    TweetService tweetService;
    @Autowired
    public TweetController(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @Autowired
    UserService userService;

    @RequestMapping(value = "/tweet", method = RequestMethod.POST)
    public String tweet(Tweets tweets) {
        tweets.setUser(getUserObject());

        tweets = tweetService.save(tweets);
        return "index";
    }
}
