package com.tweet.twitter.controller;

import com.tweet.twitter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.logging.Logger;

@Controller
public class LoginController {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private UserService userService;

    @Autowired
    LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login (Model model) {

        return "register";
    }
}
