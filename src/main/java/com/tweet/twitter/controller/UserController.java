package com.tweet.twitter.controller;

import com.tweet.twitter.model.Users;
import com.tweet.twitter.service.UserService;
import com.tweet.twitter.service.UserServiceIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

//    @GetMapping("/{username}")
//    public String userPage(@PathVariable("username") String username, Model model){
//        Users user = userService.getUserByUsername(username);
//        model.addAttribute("user",user);
//        return "profile";
//    }
}
