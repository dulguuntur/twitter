package com.tweet.twitter.controller;

import com.tweet.twitter.config.AuthenticationInfo;
import com.tweet.twitter.model.CustomUserDetails;
import com.tweet.twitter.model.Users;
import com.tweet.twitter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.rmi.CORBA.Util;
import java.util.logging.Logger;

@Controller
public class RegisterController extends AuthenticationInfo {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register (Model model) {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser (Users user, Model model) {
        boolean check = this.userService.getUserByEmail(user.getEmail());
        logger.info("Message : " + Boolean.toString(check));
        if (!check) {
            boolean checkRegistered = this.userService.saveOrUpdateSurvey(user);
            if (!checkRegistered) {
                model.addAttribute("error", "Cannot register user!");
            } else {
                logger.info("Successfully registered!");
            }
        } else {
            logger.info("User already registered!");
        }
        return "redirect:/register";
    }

    @RequestMapping(value = "/user/follow", method = RequestMethod.POST)
    public String followUser (Users userObject, Model model) {
        Users authUser = getUserObject();
        logger.info(authUser.getEmail());
        logger.info(userObject.getEmail());

        authUser.getFollowing().add(userObject);
        userService.saveOrUpdateSurvey(authUser);

        Authentication oldAuth= getPrincipal();
        Authentication newAuth = new UsernamePasswordAuthenticationToken(new CustomUserDetails(authUser),oldAuth.getCredentials(),oldAuth.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(newAuth);

        //userService.saveFollow(authUser, userObject);
        model.addAttribute("user", authUser);
        return "profile";
    }

    @GetMapping("/user/{username}")
    public String userPage(@PathVariable("username") String username, Model model){
        Users user = this.userService.getUserByUsername(username);
        model.addAttribute("user",user);
        return "profile";
    }
}
