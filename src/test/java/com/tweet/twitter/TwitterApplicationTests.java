package com.tweet.twitter;

import com.tweet.twitter.repo.UsersRepository;
import com.tweet.twitter.service.UserServiceIml;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TwitterApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	UsersRepository usersRepository;
	@Autowired
	UserServiceIml userServiceIml;

	@Test
	public void testCase1() {
		List list1 = usersRepository.findAll();
		List list2 = userServiceIml.getAllUsers();
		Assert.assertEquals(list1.size(), list2.size());

	}

}
